import { DataService } from './../services/data.service';
import { ClubesService } from './../services/clubes.service';
import { Jugador, Club } from '../models/clubes.interface';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})


export class PrincipalComponent implements OnInit{
  
  constructor(private clubesService : ClubesService, private dataService : DataService){  }

  aux :string
  jugador : Jugador
  club : Club

  ngOnInit(){
    this.dataService.tag$.subscribe(res =>{
      if(res != "") this.buscarMiembro(res)
    })
  }


  buscarJugador(entrada : HTMLInputElement){
    let aux : string = entrada.value.replace('#', '%23');
    this.clubesService.getJugador(aux).subscribe(response =>{
    this.jugador = response;
  })
  }
  
  buscarClub(entrada : HTMLInputElement){
    let aux : string = entrada.value.replace('#', '%23');
    this.clubesService.getClub(aux).subscribe(response =>{
      this.club = response;
  })
  }

  buscarMiembro(miembro : string){
    let aux : string = miembro.replace('#', '%23');
    this.clubesService.getJugador(aux).subscribe(response =>{
    this.jugador = response;
  })
  }

}