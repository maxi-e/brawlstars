export interface Jugador{
    tag?: string,
    name?: string,
    trophies?: number,
    highestTrophies?: number,
    expLevel?: number,
    expPoints?: number,
    isQualifiedFromChampionshipChallenge?: boolean,
    soloVictories?: number,
    duoVictories?: number,
    bestRoboRumbleTime?: number,
    bestTimeAsBigBrawler?: number
}

export interface Club{
   tag : string
   name : string
   trophies : number
   description: string
   requiredTrophies: number
   members : []
}


