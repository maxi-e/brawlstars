import { AuthInterceptorService } from './services/auth-interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PaginacionComponent } from './paginacion/paginacion.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PrincipalComponent } from './principal/principal.component';
import { ClubComponent } from './club/club.component';
import { JugadorComponent } from './jugador/jugador.component'

@NgModule({
  declarations: [
    AppComponent,
    JugadorComponent,
    PaginacionComponent,
    PrincipalComponent,
    ClubComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [
    {
    provide : HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true
   }
],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
