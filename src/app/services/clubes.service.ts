import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Club, Jugador } from '../models/clubes.interface';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ClubesService {

  constructor(private http : HttpClient ) { }

  private errorHandler(error: HttpErrorResponse) {
    alert("No existe el tag ingresado");
    return throwError(error);
  }

  getClub(numero : string) : Observable<Club>{
    return this.http.get<Club>('https://api.brawlstars.com/v1/clubs/'+ numero).pipe(catchError(this.errorHandler));
  }

  getJugador(numero : string) : Observable<Jugador>{
    return this.http.get<Jugador>('https://api.brawlstars.com/v1/players/'+ numero).pipe(catchError(this.errorHandler));
  }

}