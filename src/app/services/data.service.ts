import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private tag = new BehaviorSubject<string>('');

  tag$ = this.tag.asObservable();

  enviar(tag : string) {
    this.tag.next(tag);
  }
  constructor() { }
}
