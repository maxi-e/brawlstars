
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const token: string = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImUzZDBjNTFmLTYzM2EtNDQ5Mi1hN2M5LWFhOGJiNzNkZWVmOCIsImlhdCI6MTU4MDEzOTc1OCwic3ViIjoiZGV2ZWxvcGVyL2YxNzhjMDEwLWY4NzctOGUyOS1iZTRlLTM1ZGVjMzZhZTA4ZiIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiMTkwLjUyLjM0LjQxIl0sInR5cGUiOiJjbGllbnQifV19.ss8mmf895dfvAgP-9PqNfzoi0qAqSBK6_pTLm5dpwhPPXNTy1z0vAQ0Z6z-xTg9Vim2XnxgOTnmHzhqatThK0A';

    let request = req;

    if (token) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${ token }`
        }
      });
    }

    return next.handle(request);

  }

}