import { TestBed } from '@angular/core/testing';

import { ClubesService } from './clubes.service';

describe('ClubesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClubesService = TestBed.get(ClubesService);
    expect(service).toBeTruthy();
  });
});
