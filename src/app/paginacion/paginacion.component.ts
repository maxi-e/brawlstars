import { PrincipalComponent } from './../principal/principal.component';
import { DataService } from './../services/data.service';
import { ClubesService } from './../services/clubes.service';
import { Club } from '../models/clubes.interface';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.scss']
})
export class PaginacionComponent implements OnInit {

  @Input('club') club : Club 

  constructor(private route : ActivatedRoute, private clubesService : ClubesService,  private dataService : DataService) { }

  p : number = 1

  ngOnInit() {

  }
  
  buscarMiembro(tag : string){
    this.dataService.enviar(tag);
  }

}
