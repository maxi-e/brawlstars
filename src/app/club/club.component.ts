import { ClubesService } from './../services/clubes.service';
import { Club } from '../models/clubes.interface';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.scss']
})
export class ClubComponent implements OnInit {

  @Input('club') club : Club 

  constructor(private route : ActivatedRoute, private clubesService : ClubesService) { }

  p : number = 1

  ngOnInit() {

  }


}
