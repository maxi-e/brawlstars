import { ClubesService } from './../services/clubes.service';
import { Jugador } from '../models/clubes.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html',
  styleUrls: ['./jugador.component.scss']
})
export class JugadorComponent implements OnInit {

  constructor( private clubesService : ClubesService) { }

  @Input('jugador') jugador : Jugador 

  ngOnInit() {
  }

}


