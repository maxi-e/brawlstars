import { ClubComponent } from './club/club.component';
import { PrincipalComponent } from './principal/principal.component';
import { JugadorComponent } from './jugador/jugador.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router"

const routes : Routes =[
  {
    path: '', component : PrincipalComponent
  },
  {
    path: 'jugador/:id', component : JugadorComponent
  },
  {
    path: 'club', component : ClubComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
